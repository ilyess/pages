<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Tools on ilyess</title>
    <link>https://ilyess.cc/tags/tools/</link>
    <description>Recent content in Tools on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Thu, 17 Jun 2021 07:34:46 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/tools/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>My Toolbox</title>
      <link>https://ilyess.cc/uses/</link>
      <pubDate>Thu, 17 Jun 2021 07:34:46 -0400</pubDate>
      <guid>https://ilyess.cc/uses/</guid>
      <description>&lt;p&gt;First, I&amp;rsquo;d like to note that I am not affiliated in any way, shape or form,
with any of the services or corporations behind the services listed below, nor
do I have any incentives, monetary or otherwise, to use or mention any of them
here.  Second, these are not recommendations. Rather, it&amp;rsquo;s a list of
applications and services I use based on my needs.  It&amp;rsquo;s always recommended to
carefully read and understand the terms and conditions of any service before
using it.&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>First, I&rsquo;d like to note that I am not affiliated in any way, shape or form,
with any of the services or corporations behind the services listed below, nor
do I have any incentives, monetary or otherwise, to use or mention any of them
here.  Second, these are not recommendations. Rather, it&rsquo;s a list of
applications and services I use based on my needs.  It&rsquo;s always recommended to
carefully read and understand the terms and conditions of any service before
using it.</p>
<p>All the software listed below is open-source and free of charge. I truly believe
that the ability to inspect and audit the source code is crucial to preserve
users&rsquo; online security and privacy by ensuring that the practices claimed by a
given application are respected.</p>
<h2 id="browser">Browser</h2>
<p>My favourite browser of all times is <a href="/posts/why-i-love-firefox">Firefox</a>. It&rsquo;s the best out there in terms
of user privacy protection, especially when hardened with few setting tweaks and
a handful of add-ons. Here&rsquo;s a list of the extensions I use with Firefox:</p>
<ul>
<li><a href="https://ublockorigin.com/">UBlock Origin</a>: It blocks Ads, tracking, and other annoyances on the web. It
comes with a predefined Ad-list with the option of adding custom ones.</li>
<li><a href="https://github.com/ClearURLs/Addon">ClearURLs</a>: It removes all tracking query parameters from URLs before the
browser calls them.</li>
<li><a href="https://github.com/cowlicks/privacypossum">Privacy Possum</a>: It blocks the <code>referrer</code> header and <code>etag</code> tracking, as well as
some tracking cookies.</li>
<li><a href="https://decentraleyes.org/">Decentraleyes</a>: It emulates a local Content Delivery Network (CDN) by keeping a
local cache of all assets that the browser fetches from remote locations. With
this extension, the browser doesn&rsquo;t need to talk to CDNs as often as it would
without it, which reduces CDNs&rsquo; tracking ability.</li>
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/">Multi-Account Containers</a>: It allows the compartmentalization of tabs in
separate containers, effectively acting as different browser sessions. It&rsquo;s
like private browsing on steroids.</li>
</ul>
<h2 id="email">Email</h2>
<p>My main email provider is <a href="https://proton.me/mail">ProtonMail</a>. All emails within ProtonMail are end-to-end
encrypted. It&rsquo;s important to note that this only applies to the email body, the
subject is not encrypted end to end.</p>
<p>I use <a href="https://www.thunderbird.net">Thunderbird</a> as an email client whenever possible. It&rsquo;s an open source
project originally started by Mozilla, the same entity behind Firefox. It can&rsquo;t
be used with ProtonMail without running a bridge because of the encryption, so I
stick to the web when accessing my personal ProtonMail account. But for work,
where unfortunately I have no choice but to use Gmail, I connect to my account
through Thunderbird.</p>
<h2 id="instant-messaging">Instant Messaging</h2>
<p>My primary instant messaging service is <a href="https://signal.org/">Signal</a>, which is supported by a
non-profit organization. It encrypts <strong>everything</strong> end to end using a state-of-the-art,
peer-reviewed, quantum-resistant, and thoroughly-audited secure communication protocol called The
Signal Protocol. The application supports group chats and 1-to-1 and group video calls, which is
more than I could ask for. While it requires a phone number to register, it also support usernames
as user identifiers so you don&rsquo;t have to give out your phone number to connect with people on the
platform.</p>
<p>My backup instant messaging service is <a href="https://getsession.org/">Session</a>. It&rsquo;s a fork of Signal that relies on randomly
generated ids to identify users. The service is decentralized and routes all communications through
an onion network of community-managed nodes. I only use it with a couple of contacts in the rare
occasions where Signal is down.</p>
<h2 id="notes">Notes</h2>
<p>I use the default notes app on my phone with cloud backup disabled, and <a href="https://www.vim.org">vim</a>
with <a href="https://vimwiki.github.io">vimwiki</a> on the desktop.</p>
<h2 id="password-manager">Password Manager</h2>
<p>My password manager of choice is <a href="https://bitwarden.com/">Bitwarden</a>. Given that it&rsquo;s open-source, I run
my own instance so I have control over all my data. It&rsquo;s not very complicated to
do and I encourage everyone to give it a try, especially if you already have a
server (a Raspberry Pi or a VPS). Feel free to checkout <a href="https://ilyess.cc/posts/self-host-bitwarden-using-docker/">this article</a> where I go
over my setup if you&rsquo;re curious about how it&rsquo;s done. Besides, even if I wasn&rsquo;t
able to run my own instance I would still have gone with Bitwarden. To date,
it&rsquo;s the only reputable and open-source password manager with a decent free plan
offering.</p>
<h2 id="totp">TOTP</h2>
<p>To store my Time-based One-Time Passwords, I use <a href="https://ente.io/auth/">Ente Auth</a>. It has all the basics
you would expect from a TOTP app and it&rsquo;s completely open source.</p>
<h2 id="social">Social</h2>
<p>I&rsquo;m not on any of the common social media platforms like Facebook, Instagram,
and Twitter. Instead, I prefer to socialize on <a href="https://joinmastodon.org">Mastodon</a> and share photos on
<a href="https://pixelfed.org">Pixelfed</a>. Both of these platforms are on the <a href="https://en.wikipedia.org/wiki/Fediverse">Fediverse</a>, meaning their codebase
is open-source and they are federated.</p>
]]></content:encoded>
    </item>
  </channel>
</rss>
