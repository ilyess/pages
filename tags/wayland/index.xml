<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Wayland on ilyess</title>
    <link>https://ilyess.cc/tags/wayland/</link>
    <description>Recent content in Wayland on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Sun, 05 Jan 2025 03:00:00 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/wayland/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Nvidia Finally Working on Wayland</title>
      <link>https://ilyess.cc/posts/nvidia-finally-working-on-wayland/</link>
      <pubDate>Fri, 03 Jan 2025 03:00:00 -0400</pubDate>
      <guid>https://ilyess.cc/posts/nvidia-finally-working-on-wayland/</guid>
      <description>&lt;p&gt;I&amp;rsquo;ve been on &lt;a href=&#34;https://archlinux.org/&#34;&gt;Arch Linux&lt;/a&gt; for years now and if there&amp;rsquo;s one thing I miss from the old Windows days,
it&amp;rsquo;s got to be gaming. I&amp;rsquo;m not a professional gamer by any stretch of the imagination, or a gamer
period, for that matter. I just like to check out some titles here and there, mainly for immersive
experiences, and the occasional intellectual puzzle. If I were to guess, I&amp;rsquo;d put my average play
time around the 2-to-3h-per-month ballpark. So, &amp;ldquo;sacrificing&amp;rdquo; gaming for the benefit of a better
operating system was a no-brainer. I took the plunge the moment I could - no regrets there. That
being said, gaming was always at the back of my mind. I&amp;rsquo;d see new games come out, or gameplay
recordings of old favorite games of mine, and just wish I could whip out my Fortnite costume and
sink one or two hours in an adrenaline-spiking FPS game. And no, I don&amp;rsquo;t actually have a gaming
costume - it&amp;rsquo;s just an expression. I don&amp;rsquo;t even remember ever playing Fortnite, come to think of it.
Do you think I should give it a try?&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>I&rsquo;ve been on <a href="https://archlinux.org/">Arch Linux</a> for years now and if there&rsquo;s one thing I miss from the old Windows days,
it&rsquo;s got to be gaming. I&rsquo;m not a professional gamer by any stretch of the imagination, or a gamer
period, for that matter. I just like to check out some titles here and there, mainly for immersive
experiences, and the occasional intellectual puzzle. If I were to guess, I&rsquo;d put my average play
time around the 2-to-3h-per-month ballpark. So, &ldquo;sacrificing&rdquo; gaming for the benefit of a better
operating system was a no-brainer. I took the plunge the moment I could - no regrets there. That
being said, gaming was always at the back of my mind. I&rsquo;d see new games come out, or gameplay
recordings of old favorite games of mine, and just wish I could whip out my Fortnite costume and
sink one or two hours in an adrenaline-spiking FPS game. And no, I don&rsquo;t actually have a gaming
costume - it&rsquo;s just an expression. I don&rsquo;t even remember ever playing Fortnite, come to think of it.
Do you think I should give it a try?</p>
<p>Fast-forward to today, or more accurately few days ago, and I finally managed to get my NVIDIA card
working on Arch Linux and <a href="https://wayland.freedesktop.org/">Wayland</a> with no hacky workarounds or precarious maintenance routines. It
just works! It worked right after I sorted everything out, it works after reboots, it&rsquo;s working
right now as I type these words on my terminal that renders through the dedicated graphics card
(yes, you can have your terminal run on the GPU - maybe it&rsquo;s a topic for another post), and should
continue working after driver updates that are managed by my package manager! Can you believe it?
All right, I&rsquo;m getting ahead of myself here - I got to tame my excitement, and tell you how I got
here.</p>
<h2 id="the-struggle">The Struggle</h2>
<p>I&rsquo;ve been using open-source graphic drivers that support NVIDIA, like <a href="https://wiki.archlinux.org/title/Nouveau">Nouveau</a>, for the bigger part
of my time on Linux. The problem was&hellip; they all suck, if they work at all. As a matter of fact, I
was inadvertently using the integrated GPU with the dedicated one completely off for so long without
noticing. That&rsquo;s how comparable NVIDIA&rsquo;s performance with open-source drivers was to that of just
plain simple integrated GPU. Watching my NVIDIA card sit there and collect dust never sat well with
me however. At first, whenever I had some free time I would take another stab at making it work. I
had some noticeable success when I was on <a href="https://en.wikipedia.org/wiki/X_Window_System">X11</a> by using the official proprietary driver straight from
NVIDIA&rsquo;s website. It worked well with few caveats that I was ready to put up with for the sake of
an improved graphic experience.</p>
<p>Sadly, when I switched to Wayland, all hell broke loose. Those two (NVIDIA and Wayland) did not like
one another - they actively despised and energetically repelled each other. It was like trying to
sit a magnet on top of another with similar poles facing each other. I remember once diving so deep
in the rabbit hole that I found the line in Gnome&rsquo;s source code where they explicitly ignore
everything that has to do with NVIDIA when Wayland is loaded. Yeah, it was <em>that</em> bad! Shortly after
this attempt, with a vivid memory of so many failures, black screens, frozen desktop environments,
bricked systems, and geometrically paradoxical screen resolutions, I silently gave up. I felt like
my hands were tied and that there wasn&rsquo;t much I could do to marry NVIDIA and Wayland on my machine.
This isn&rsquo;t to say that it was impossible. I&rsquo;d read multiple testimonials of people enjoying the full
performance of their NVIDIA cards with Wayland, but there was something with my system that made
reproducing a similar configuration challenging, to say the least. But, this all changed few days
ago.</p>
<h2 id="the-win">The Win</h2>
<p>A short while ago, I randomly decided it was time I tried to sort out my NVIDIA+Wayland situation. I
don&rsquo;t remember exactly what triggered it. The trauma from previous attempts had started to fade
away, I suppose, and I had some time on my hands so I thought &ldquo;Why not?&rdquo;. I headed over to the
trusty <a href="https://wiki.archlinux.org/title/Main_page">Arch Wiki</a>, read through the NVIDIA section, and diligently followed
the steps that applied to my system and configuration. Surprisingly, all I ended up doing was
installing the official NVIDIA driver from the official Arch package repository (this wasn&rsquo;t
available last time I tried by the way, so it gave me hope on the spot), and updating the
<code>/etc/mkinitcpio.conf</code> file to remove <code>kms</code> from the <code>HOOKS</code> array. Et voila! I rebooted my system
and there it was - the NVIDIA card was working! I could see it in the <code>nvidia-smi</code> output, and I
could feel it in the smoothness of the animations and the responsiveness of the system. I was
ecstatic! I couldn&rsquo;t believe it was that easy. I was so used to the idea that getting NVIDIA to work
on Wayland was a monumental task that I had to prepare myself for, that I didn&rsquo;t even consider the
possibility that it could be as simple as installing a package and updating a configuration file. I
was over the moon! But, I wasn&rsquo;t done yet.</p>
<p>Now that I had the NVIDIA card properly configured, I wanted to make the most out of it. But I
didn&rsquo;t want to use it all the time. I wanted to keep the hybrid setup I had going on with the
integrated GPU handling the day-to-day tasks and the NVIDIA card kicking in for the heavy lifting
when strenuous rendering was needed. The default hybrid mode was doing a pretty good job at this,
but I wanted to take it a step further. I wanted to force the NVIDIA card to render specific apps
that I knew would benefit from the extra power, like Firefox when watching videos. I spent quite a
bit of time on this and I&rsquo;m happy to say that I managed to get it working. I used the <code>prime-run</code>
command to launch the apps I wanted to render with the NVIDIA card, and it worked like a charm. I
was hoping to accomplish this without manually updating the launch command of each app, but I
couldn&rsquo;t find a way to do it. If you know of a different way to achieve this, please let me know via
<a href="mailto:hi%40ilyess.cc?subject=RE:%20Nvidia%20Finally%20Working%20on%20Wayland">email</a> or on <a href="https://mastodon.online/@ilyess/113766705195593318">Mastodon</a>.</p>
<h2 id="whats-next">What&rsquo;s Next</h2>
<p>I&rsquo;m not done yet, unfortunately. Nothing needs a powerful GPU more than games, and I haven&rsquo;t had the
chance to test any games yet. I can finally satiate my envy of Linux gamers and see what all the
fuss is about. I&rsquo;m excited to see how games run on my system. I&rsquo;m not expecting to be blown away by
the performance, but I&rsquo;m hoping to have a decent experience.</p>
<p>Also, I still want to move back to a <a href="https://en.wikipedia.org/wiki/Window_manager">window manager</a>. I&rsquo;ve been using Gnome for a while now because I
really wanted to switch to Wayland, even if it meant forfeiting my beloved window manager. But now
that I have successfully configured NVIDIA to function seamlessly on Wayland, I&rsquo;m finally able to
return to the refined simplicity of a window manager. I have my eyes set on Sway. Having heard
numerous commendations, I&rsquo;m eager to explore its merits and experience it firsthand. I&rsquo;ll probably
publish a post on the subject once I&rsquo;ve had some time to play around with Sway. So, stay tuned<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup> for
that.</p>
<h2 id="wrap-up">Wrap Up</h2>
<p>My goal with this post was to give you a glimpse into my journey with NVIDIA on Wayland. I wanted to
share my struggles, my wins, and my plans for the future. I hope you found it interesting and maybe
even helpful. If you&rsquo;re in a similar situation, I hope this post gives you hope that you can get
NVIDIA working on Wayland too. If you have any suggestions, tips, or questions, please don&rsquo;t
hesitate to reach out on <a href="https://mastodon.online/@ilyess/113766705195593318">Mastodon</a> or via <a href="mailto:hi%40ilyess.cc?subject=RE:%20Nvidia%20Finally%20Working%20on%20Wayland">email</a>. I&rsquo;d love to hear from you. Until next time, take
care and Happy New Year!</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>Feel free to subscribe to my blog&rsquo;s <a href="https://ilyess.cc/posts/index.xml">RSS feed</a> to get notified when I publish new posts.&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
