<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Blogging on ilyess</title>
    <link>https://ilyess.cc/tags/blogging/</link>
    <description>Recent content in Blogging on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Tue, 14 Feb 2023 20:00:00 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/blogging/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Your Blog Does Not Need a Database</title>
      <link>https://ilyess.cc/posts/your-blog-does-not-need-a-database/</link>
      <pubDate>Tue, 14 Feb 2023 20:00:00 -0400</pubDate>
      <guid>https://ilyess.cc/posts/your-blog-does-not-need-a-database/</guid>
      <description>&lt;p&gt;In the present digital epoch, it has become imperative for companies with an online presence to host
a blog which is nothing more than a compendium of articles or posts, typically organized
chronologically. This is not limited to companies obviously and anyone can, and is even highly
encouraged to, run a personal blog. If you&amp;rsquo;re one of the fortunate who own a personal blog and are
currently relying on a database to power it, you may be under the impression that it&amp;rsquo;s necessary for
your blog to function properly. Wrong! In this article I&amp;rsquo;m going to explain why that&amp;rsquo;s not the case
and why your blog doesn&amp;rsquo;t actually need a database. I will even go further and suggest that your
blog &lt;em&gt;should not&lt;/em&gt; use a database. While databases can be useful, if not mandatory, for certain types
of websites, blogs are definitely not one of them.&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>In the present digital epoch, it has become imperative for companies with an online presence to host
a blog which is nothing more than a compendium of articles or posts, typically organized
chronologically. This is not limited to companies obviously and anyone can, and is even highly
encouraged to, run a personal blog. If you&rsquo;re one of the fortunate who own a personal blog and are
currently relying on a database to power it, you may be under the impression that it&rsquo;s necessary for
your blog to function properly. Wrong! In this article I&rsquo;m going to explain why that&rsquo;s not the case
and why your blog doesn&rsquo;t actually need a database. I will even go further and suggest that your
blog <em>should not</em> use a database. While databases can be useful, if not mandatory, for certain types
of websites, blogs are definitely not one of them.</p>
<p>One of the main reasons why blogs don&rsquo;t need a database is that they are typically static websites.
This means that the content on the website does not change frequently and isn&rsquo;t customized for any
particular visitor, and the website does not require any user interaction. In such a case, a
database would be unnecessary as the information on the website can be stored in the form of HTML
and CSS files.</p>
<p>Before going further, if your website has any dynamic functionality like user
registration and login, or first party comment support, you&rsquo;re out of luck and there&rsquo;s no way around
using a database; unless you&rsquo;re feeling adventurous enough to delegate those functionalities to
third party providers and hook your website onto them solely through the frontend. This adds a bit
of complexity and calls for some technical knowledge though, which voids some of the benefits of
static websites. Maybe you don&rsquo;t <em>need</em> those dynamic functionalities to begin with and might be
better off without them. Food for thought.</p>
<h2 id="the-old-school">The Old School</h2>
<p>The traditional method of running a blog is by utilizing a database to store the articles and a
server-side script that dynamically generates the pages upon a user&rsquo;s request. This method, commonly
referred to as a &ldquo;dynamic&rdquo; approach, has been the prevalent norm for a considerable duration.
However, this has several drawbacks.</p>
<p>First, the energy consumption is a paramount concern. Databases necessitate a substantial
computational capacity, and oftentimes, they are run on servers that consume copious amounts of
energy. This energy consumption leads to a substantial carbon footprint, thereby exacerbating
climate change. Furthermore, the servers that host these databases are usually situated in data
centers, which consume substantial amounts of energy to cool the servers and keep them operational.
In addition, databases are frequently over-provisioned and underutilized, resulting in the squander
of resources. This is a direct result of the fact that a database must be able to handle the peak
load, even though the actual load is frequently much lower. This is a significant waste of
resources, both in terms of energy and hardware.</p>
<p>Another drawback of using a database is the cost. Hosting a dynamic blog requires a powerful server,
as well as a database. This can be expensive, especially for personal blogs which are not typically
meant to generate revenue. Additionally, the more traffic a blog receives, the more powerful the
server must be, and the more resources are required to handle the traffic. This can make it
difficult for a blog to scale, and can also be a significant barrier to entry for those who are just
starting out and do not have a lot of resources.</p>
<p>The third drawback of using a database is the slower page load times. When a user requests a page on
a dynamic blog, the server must first query the database to retrieve the data, and then execute the
server-side scripts to generate the page. This process takes time, and the more traffic a blog
receives, the longer it takes to generate the pages. This can lead to a poor user experience, and
can also negatively impact the blog&rsquo;s search engine rankings.</p>
<h2 id="the-hot-new-thing">The Hot New Thing</h2>
<p>An alternative approach is to use a statically generated blog, which pre-generates all the pages and
serves them as plain HTML and CSS files. This approach has several advantages over using a database
besides the obvious benefits of not carrying the aforementioned drawbacks.</p>
<p>First, since the pages are pre-generated, they can be served directly by a web server without the
need for any dynamic processing. This means that the hosting costs are lower, as you don&rsquo;t need a
powerful server or a database. Additionally, the static pages can be served directly by a Content
Delivery Network (CDN), which can further reduce the hosting costs, and are often cached by the
browser which also improves the page load times.</p>
<p>Another advantage of using a statically generated blog is the ability to generate pages offline.
With a dynamic blog, the pages are generated on the fly, which means that the server must be online
and accessible in order for the website to function. With a statically generated blog, however, the
pages can be generated offline, and then uploaded to the web server. This can be useful for testing
and brings a high degree of portability. Given that the website is nothing but static files, you can
almost instantly move your website from one hosting provider to another.</p>
<p>The third advantage of using a statically generated blog is the increased security. With a dynamic
blog, there is always a risk that the server-side scripts or the database could be hacked, which
could potentially lead to sensitive information being compromised. With a statically generated blog,
on the other hand, there is no server-side script or database to hack, which reduces the risk of a
security breach.</p>
<h2 id="i-dont-want-to-deal-with-html-and-css">I Don&rsquo;t Want To Deal With HTML And CSS</h2>
<p>If the thought of having to manually create and manipulate HTML and CSS files makes you sweat,
don&rsquo;t you worry. Using a statically generated website doesn&rsquo;t mean that you need to become an
proficient frontend developer. There are tools, so called static site generators (SSG), that allow
you to create your blog using simple text files, such as Markdown, and then generate the necessary
HTML and CSS files. This means that you can exclusively focus on creating content and customizing
the design of your blog, by leveraging off-the-shelf themes.</p>
<p>One of the most popular SSG is <a href="https://jekyllrb.com">Jekyll</a>, which is written in Ruby. Jekyll takes your content, written
in Markdown or Textile, and uses layouts to create a static website. Jekyll also supports data
files, which can be used to store data in a structured format, such as YAML or JSON, that can be
accessed in your templates.</p>
<p>Another popular SSG is <a href="https://gohugo.io">Hugo</a>, which is written in Go. It is a fast and flexible tool that can be used
to build a wide variety of websites, including blogs. Hugo uses markdown files to generate your
website, and it supports data files and template variables, which can be used to store and access
data in a structured format.</p>
<p>It goes without saying that there are many more static site generators to choose from. Once the
static website files are generated, they need to be hosted by a web hosting provider for the website
to go online and be accessible from the internet. Here again, there&rsquo;s no shortage of web hosting
providers on the market. In fact, many would even be happy to host your static website for free!
Providers like <a href="https://www.netlify.com">Netlify</a>, <a href="https://codeberg.page/">Codeberg Pages</a>, <a href="https://pages.github.com/">Github Pages</a>, and <a href="https://vercel.com/">Vercel</a> immediately come to mind but I&rsquo;m
sure there are plenty others.</p>
<h2 id="wrap-up">Wrap up</h2>
<p>In today&rsquo;s world, environmental sustainability is a top priority. And when it comes to blogs,
statically generated sites are the clear winner. They require less energy and computing power, are
much more secure than their dynamic counterparts, and are much easier to maintain. While I doubt
that we can solve all our climate problems by making blogs static, I do believe that every bit of
contribution towards that goal helps, however minute it seems. So if you&rsquo;re looking to reduce your
environmental impact and create a secure, cost-effective blog, be sure to go static!</p>
]]></content:encoded>
    </item>
  </channel>
</rss>
