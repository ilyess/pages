<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Bitwarden on ilyess</title>
    <link>https://ilyess.cc/tags/bitwarden/</link>
    <description>Recent content in Bitwarden on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Wed, 02 Jun 2021 20:00:00 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/bitwarden/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Self-Host Bitwarden using Docker</title>
      <link>https://ilyess.cc/posts/self-host-bitwarden-using-docker/</link>
      <pubDate>Wed, 02 Jun 2021 20:00:00 -0400</pubDate>
      <guid>https://ilyess.cc/posts/self-host-bitwarden-using-docker/</guid>
      <description>&lt;p&gt;&lt;a href=&#34;https://bitwarden.com&#34;&gt;Bitwarden&lt;/a&gt; is a password manager that allows users to generate and store strong
passwords. It also handles other types of data like secure notes and credit card
information. At the time of this writing, it is one of the best password
managers out there because in addition to offering strong and zero-knowledge
encryption, the codebase is &lt;a href=&#34;https://bitwarden.com/open-source/&#34;&gt;open source&lt;/a&gt;. This means that anyone can inspect the
code and run it for their personal use. In this article, we&amp;rsquo;ll go over the steps
to build a fully functioning Bitwarden instance that anyone can run on a server
at home.&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p><a href="https://bitwarden.com">Bitwarden</a> is a password manager that allows users to generate and store strong
passwords. It also handles other types of data like secure notes and credit card
information. At the time of this writing, it is one of the best password
managers out there because in addition to offering strong and zero-knowledge
encryption, the codebase is <a href="https://bitwarden.com/open-source/">open source</a>. This means that anyone can inspect the
code and run it for their personal use. In this article, we&rsquo;ll go over the steps
to build a fully functioning Bitwarden instance that anyone can run on a server
at home.</p>
<h2 id="docker-setup">Docker setup</h2>
<p>Let&rsquo;s start by setting up a <code>docker-compose.yml</code> file to run the Bitwarden server:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl">version: <span class="s2">&#34;3.8&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>services:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  bitwarden:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    container_name: <span class="s2">&#34;bitwarden&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    image: vaultwarden/server<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    ports:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="s2">&#34;3012:3012&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - data:/data<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  data:<span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><p>Here, we&rsquo;re using vaultwarden/server image which is a rust-based version of
Bitwarden server. We&rsquo;re exposing a TCP port to be able to communicate with the
container from outside of Docker network. Then, we create a volume and mount it
on /data inside the container. That&rsquo;s where Bitwarden stores its data, including
users, vaults, and attachments.</p>
<p>If we bring up this container and try to access Bitwarden through
<code>https://localhost:3012</code>, it will not work.  Bitwarden requires all communications
to go through a TLS tunnel. In other words, we need to use HTTPS instead of
HTTP. We&rsquo;re going to use a second container running Nginx to handle TLS for us
and proxy requests to Bitwarden&rsquo;s container.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span><span class="lnt">14
</span><span class="lnt">15
</span><span class="lnt">16
</span><span class="lnt">17
</span><span class="lnt">18
</span><span class="lnt">19
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl">version: <span class="s2">&#34;3.8&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>services:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  bitwarden:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    depends_on:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - nginx<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  nginx:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    image: nginx:1.21.0-alpine<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    ports:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="s2">&#34;23984:443&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - ./nginx/ssl:/etc/ssl:ro<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - ./nginx/conf.d:/etc/nginx/conf.d:ro<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    restart: unless-stopped<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  data:<span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><p>In this configuration, we&rsquo;re declaring a new service <code>nginx</code> using the alpine
image of <code>nginx</code>, exposing a port so that the container can be reacher from
outside Docker&rsquo;s internal network, and specifying a couple of read-only volumes.
We&rsquo;re also instructing Docker to automatically restart this container unless it
was manually stopped. This can help increase availability by automatically
recovering from sudden crashes.</p>
<p>The final result looks like follows. Notice that we introduced a environment
variable <code>ADMIN_TOKEN</code> that can be used to access the admin section of
Bitwarden. More on how to do this later.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span><span class="lnt">14
</span><span class="lnt">15
</span><span class="lnt">16
</span><span class="lnt">17
</span><span class="lnt">18
</span><span class="lnt">19
</span><span class="lnt">20
</span><span class="lnt">21
</span><span class="lnt">22
</span><span class="lnt">23
</span><span class="lnt">24
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl">version: <span class="s2">&#34;3.8&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>services:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  bitwarden:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    container_name: <span class="s2">&#34;bitwarden&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    image: vaultwarden/server<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    environment:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="nv">ADMIN_TOKEN</span><span class="o">=</span>SOME_SECRET_TOKEN<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - data:/data<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    depends_on:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - nginx<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  nginx:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    image: nginx:1.21.0-alpine<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    ports:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="s2">&#34;23984:443&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - ./nginx/ssl:/etc/ssl:ro<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - ./nginx/conf.d:/etc/nginx/conf.d:ro<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    restart: unless-stopped<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  data:<span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><h2 id="tls-support">TLS Support</h2>
<p>Now that we have our containers set up, it&rsquo;s time to configure Nginx to handle
TLS connections and forward them to Bitwarden&rsquo;s container. Let&rsquo;s create a
server configuration file named <code>bitwarden.conf</code> and place it under
<code>nginx/conf.d/</code>. This path relative to wherever you place the
<code>docker-compose.yml</code> file.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span><span class="lnt">14
</span><span class="lnt">15
</span><span class="lnt">16
</span><span class="lnt">17
</span><span class="lnt">18
</span><span class="lnt">19
</span><span class="lnt">20
</span><span class="lnt">21
</span><span class="lnt">22
</span><span class="lnt">23
</span><span class="lnt">24
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-fallback" data-lang="fallback"><span class="line"><span class="cl">server {
</span></span><span class="line"><span class="cl">    listen 443 ssl http2;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    # Allow large attachments
</span></span><span class="line"><span class="cl">    client_max_body_size 128M;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location / {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:80;
</span></span><span class="line"><span class="cl">        proxy_set_header Host $host;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Real-IP $remote_addr;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Forwarded-Proto $scheme;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location /notifications/hub {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:3012;
</span></span><span class="line"><span class="cl">        proxy_set_header Upgrade $http_upgrade;
</span></span><span class="line"><span class="cl">        proxy_set_header Connection &#34;upgrade&#34;;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location /notifications/hub/negotiate {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:80;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">}
</span></span></code></pre></td></tr></table>
</div>
</div><p>This is a pretty typical Nginx proxy configuration where we define a port to
listen to along with protocols we want to handle. Next, we have a directive to
allow large attachments<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup> and we declare few paths and how they should be
handled. We&rsquo;re not going to dive into each directive used here but If you want
to know more about how to configure Nginx, I encourage to check out their
<a href="https://nginx.org/en/docs/">official documentation.</a></p>
<p>If you&rsquo;ve been following along you&rsquo;ll notice that we haven&rsquo;t addressed the SSL
connection yet, and without it our setup will not work. So let&rsquo;s figure that
out. In case you&rsquo;re planning to use a real domain name for your Bitwarden instance,
you can skip this section and jump directly to <a href="https://ilyess.cc/posts/self-host-bitwarden-using-docker/#nginx-configuration">Nginx Configuration</a>.</p>
<p>In order to make a self-signed TLS certificate, first we need to generate a
private key and a certificate for our Bitwarden server using the following
command.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span><span class="lnt">2
</span><span class="lnt">3
</span><span class="lnt">4
</span><span class="lnt">5
</span><span class="lnt">6
</span><span class="lnt">7
</span><span class="lnt">8
</span><span class="lnt">9
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-sh" data-lang="sh"><span class="line"><span class="cl"><span class="nv">OUT_FOLDER</span><span class="o">=</span>/path/to/ssl
</span></span><span class="line"><span class="cl"><span class="nv">DOMAIN</span><span class="o">=</span>your.bitwarden.domain
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">openssl req -x509 -nodes -days <span class="m">365</span> -newkey rsa:4096 <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>        -config &lt;<span class="o">(</span>cat /etc/ssl/openssl.cnf &lt;<span class="o">(</span><span class="nb">printf</span> <span class="s2">&#34;[SAN]\nsubjectAltName=DNS:</span><span class="nv">$DOMAIN</span><span class="s2">\nbasicConstraints=CA:true&#34;</span><span class="o">))</span> <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>        -keyout <span class="nv">$OUT_FOLDER</span>/private/nginx-bitwarden.key <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>        -out <span class="nv">$OUT_FOLDER</span>/certs/nginx-bitwarden.cert <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>        -reqexts SAN -extensions SAN <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>        -subj <span class="s2">&#34;/C=US/ST=New York/L=New York/O=Company Name/OU=Bitwarden/CN=</span><span class="nv">$DOMAIN</span><span class="s2">&#34;</span>
</span></span></code></pre></td></tr></table>
</div>
</div><p>This will generate a new public key <code>nginx-bitwarden.key</code> using RSA with a key
length of <code>4096</code> bits, and a self-signed TLS certificate <code>nginx-bitwarden.cert</code>
valid for <code>365</code> days in the <code>private/</code> and <code>certs/</code> folders respectively. These
folders should be placed under the folder <code>ssl/</code> that lives alongside our
<code>docker-compose.yml</code> file, so make sure you set the <code>$OUT_FOLDER</code> variable
properly. In addition, the <code>$DOMAIN</code> should be set to the fully qualified domain
name of the machine where you&rsquo;re planning to run Bitwarden and Nginx. It&rsquo;s not
complicated to assign an FQDN to your local machine using a local DNS,
especially if you&rsquo;re running Pihole. If you&rsquo;re interested in getting Pihole set
up locally with Docker, check out this <a href="https://ilyess.cc/posts/pihole-dhcp-docker-bridge-network/">article</a>.</p>
<h3 id="nginx-configuration">Nginx Configuration</h3>
<p>Now that we have our certificate and private key, we should tell Nginx where
they are located in order to use them for TLS connections.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span><span class="lnt">2
</span><span class="lnt">3
</span><span class="lnt">4
</span><span class="lnt">5
</span><span class="lnt">6
</span><span class="lnt">7
</span><span class="lnt">8
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-fallback" data-lang="fallback"><span class="line"><span class="cl">server {
</span></span><span class="line"><span class="cl">    listen 443 ssl http2;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    ssl_certificate      /etc/ssl/certs/nginx-bitwarden.crt;
</span></span><span class="line"><span class="cl">    ssl_certificate_key  /etc/ssl/private/nginx-bitwarden.key;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    # ...
</span></span><span class="line"><span class="cl">}
</span></span></code></pre></td></tr></table>
</div>
</div><p>It&rsquo;s recommended to generate a Diffie-Hellman file for stronger connections and
use it in our Nginx configuration. We can generate one with the command:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-sh" data-lang="sh"><span class="line"><span class="cl">openssl dhparam -out <span class="nv">$OUT_FOLDER</span>/certs/dhparam.pem <span class="m">4096</span>
</span></span></code></pre></td></tr></table>
</div>
</div><p>To be honest, I haven&rsquo;t yet looked at the functions of this Diffie-Hellman file
to fully understand how it improves security. But if you know more about it
and are interested in contributing your knowledge to this article, feel free to
reach out to me on <a href="https://mastodon.online/@ilyess">Mastodon</a>.</p>
<p>The complete Nginx configuration after linking the <code>.pem</code> file should look like
follows.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span><span class="lnt">14
</span><span class="lnt">15
</span><span class="lnt">16
</span><span class="lnt">17
</span><span class="lnt">18
</span><span class="lnt">19
</span><span class="lnt">20
</span><span class="lnt">21
</span><span class="lnt">22
</span><span class="lnt">23
</span><span class="lnt">24
</span><span class="lnt">25
</span><span class="lnt">26
</span><span class="lnt">27
</span><span class="lnt">28
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-fallback" data-lang="fallback"><span class="line"><span class="cl">server {
</span></span><span class="line"><span class="cl">    listen 443 ssl http2;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    ssl_certificate      /etc/ssl/certs/nginx-bitwarden.crt;
</span></span><span class="line"><span class="cl">    ssl_certificate_key  /etc/ssl/private/nginx-bitwarden.key;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    ssl_dhparam /etc/ssl/certs/dhparam.pem;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    client_max_body_size 128M;
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location / {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:80;
</span></span><span class="line"><span class="cl">        proxy_set_header Host $host;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Real-IP $remote_addr;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
</span></span><span class="line"><span class="cl">        proxy_set_header X-Forwarded-Proto $scheme;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location /notifications/hub {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:3012;
</span></span><span class="line"><span class="cl">        proxy_set_header Upgrade $http_upgrade;
</span></span><span class="line"><span class="cl">        proxy_set_header Connection &#34;upgrade&#34;;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">    location /notifications/hub/negotiate {
</span></span><span class="line"><span class="cl">        proxy_pass http://bitwarden:80;
</span></span><span class="line"><span class="cl">    }
</span></span><span class="line"><span class="cl">}
</span></span></code></pre></td></tr></table>
</div>
</div><p>Finally, we have our containers configured to run Bitwarden. All we need do to
in order to bring them up is run:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-sh" data-lang="sh"><span class="line"><span class="cl">docker-compose up -d bitwarden
</span></span></code></pre></td></tr></table>
</div>
</div><p>Once the containers are up, you&rsquo;ll be able to reach Bitwarden through its domain
name. Given our example configuration, that would be
<code>https://your.bitwarden.domain</code>. The first time you open the page, your browser
will most likely complain about an insecure connection and display a flashy
warning.  If it doesn&rsquo;t, stop using this browser and get yourself a real one!
This is due to the self-signed certificate. Since the certificate is not signed
by a Certificate Authority (CA) present in the browsers or the OS&rsquo;s root CA
store, the browser considers the connection insecure. You can add an exception
for this certificate in your browser so that it doesn&rsquo;t freak out every time you
access your self-hosted Bitwarden. Similarly, you will have to install this
certificate on your device and trust it, otherwise the OS will prevent
Bitwarden&rsquo;s app to communicate with our running container. To do so, send the
<code>nginx-bitwarden.crt</code> file we generated in the <a href="https://ilyess.cc/posts/self-host-bitwarden-using-docker/#tls-support">TLS Support section</a> to your
device<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup>, open it and follow the installation steps. It should be
straightforward in most OS&rsquo;s but it&rsquo;s always safe to follow the official guide
to make sure you cover all the steps. For instance, in iOS, on top of installing
the certificate you have to explicitly trust it for TLS connections.</p>
<h2 id="admin-access">Admin Access</h2>
<p>Like we&rsquo;ve seen in a previous section, in order to unlock the admin panel we
need to set a token in the <code>ADMIN_TOKEN</code> environment variable. Let&rsquo;s generate a
token using the following command.</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-sh" data-lang="sh"><span class="line"><span class="cl">openssl rand -base64 <span class="m">48</span>
</span></span></code></pre></td></tr></table>
</div>
</div><p>Once Bitwarden&rsquo;s container is restarted after setting the admin token, you can
access the admin panel at <code>https://your.bitwarden.domain/admin</code>.</p>
<h2 id="wrap-up">Wrap up</h2>
<p>We finally made it. We built a docker setup running a container for the
Bitwarden server with an Nginx proxy handling TLS connections using a
self-signed certificate. This grants us full control over our credentials and
any other data we choose to store in Bitwarden, all without having to trust any
third party to handle this for us.</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>This one is optional and is only necessary if you use Bitwarden for
attachments or to send large files.&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2">
<p>It&rsquo;s safe to use email here since the certificate doesn&rsquo;t contain any
sensitive information.&#160;<a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
