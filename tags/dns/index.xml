<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Dns on ilyess</title>
    <link>https://ilyess.cc/tags/dns/</link>
    <description>Recent content in Dns on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Tue, 15 Feb 2022 20:00:00 -0500</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/dns/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>DNS over HTTPS in Pihole with Docker</title>
      <link>https://ilyess.cc/posts/dns-over-https-in-pihole-with-docker/</link>
      <pubDate>Tue, 15 Feb 2022 20:00:00 -0500</pubDate>
      <guid>https://ilyess.cc/posts/dns-over-https-in-pihole-with-docker/</guid>
      <description>&lt;p&gt;There&amp;rsquo;s a saying that goes: &amp;ldquo;Show me your friends, I&amp;rsquo;ll tell you who you are&amp;rdquo;. A
slight variation of this is: &amp;ldquo;Show me the websites you visit, I&amp;rsquo;ll tell you who
you are&amp;rdquo;. A lot can be learned about an individual just by examining the websites
they visit, the search queries they run, and the apps they use on a regular
basis. A trove of information about a user&amp;rsquo;s online activity can be gleaned from
their DNS traffic. Have you ever wondered why practically all Internet Service
Providers (ISPs) pre-configure consumer routers with their own DNS servers?&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>There&rsquo;s a saying that goes: &ldquo;Show me your friends, I&rsquo;ll tell you who you are&rdquo;. A
slight variation of this is: &ldquo;Show me the websites you visit, I&rsquo;ll tell you who
you are&rdquo;. A lot can be learned about an individual just by examining the websites
they visit, the search queries they run, and the apps they use on a regular
basis. A trove of information about a user&rsquo;s online activity can be gleaned from
their DNS traffic. Have you ever wondered why practically all Internet Service
Providers (ISPs) pre-configure consumer routers with their own DNS servers?</p>
<p>In this article, we&rsquo;ll learned a bit about DoH with a quick refresher on DNS and
how it works, and go over a few strategies to improve online privacy by securing
DNS.</p>
<h2 id="whats-doh">What&rsquo;s DoH?</h2>
<p>The Domain Name System (DNS) is the system used to identify computers on the
internet. Computers can only communicate with one another if they know each
other&rsquo;s IP addresses. But IP addresses, which are basically a bunch of numbers
bundled together, are hard to memorize. If we had to use IP addresses to access
websites, the web wouldn&rsquo;t have taken off the way it did and you wouldn&rsquo;t be
reading this article online today. DNS was invented to accommodate humans, not
machines.</p>
<p>Every time you type a URL in the browser, it issues a DNS a query to your DNS
server in order to resolve the IP address of the website you&rsquo;re trying to visit.
The same thing happens when you open an app on your mobile device, when your
smart light bulb phones home to fetch the most up-to-date brightness level it
should shine at, and when you add a new show to your list on your smart TV
Netflix app. Basically, every single action you make online triggers one or more
DNS queries.</p>
<p>However, DNS was not built with privacy in mind. All DNS queries are routed
through the internet in plain text. This means that anyone sniffing traffic on a
network, or acting as a proxy to the internet, like an ISP or an enterprise
router, can see the web locations that everyone on that network is visiting.
While watching DNS traffic alone doesn&rsquo;t give out information about the
interactions between a client and a given website, it does paint a detailed
picture of the web locations that a user has visited, an estimate of how long
they have been on each website, the apps they&rsquo;re using, and the type of
Internet-of-Thing (IoT) devices they have installed in their homes, if any. It
goes without saying that this is extremely detrimental to one&rsquo;s online privacy.
Fortunately, there are solutions to this problem.</p>
<p>One way of protecting one&rsquo;s DNS queries from snooping eyes is to use encryption,
like with DNS over TLS (DoT) or DNS over HTTPS (DoT). In this article we&rsquo;ll
focus on DoH which routes DNS traffic in an encrypted HTTPS tunnel. The client
establishes a secure connection with the DNS server and funnels all DNS queries
through it. This effectively encrypts DNS communications and renders them
inaccessible to spying third parties. Now, let&rsquo;s explore a few options of
leveraging DoH to protect online activities<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup> and improve privacy.</p>
<h2 id="client-settings">Client Settings</h2>
<p>Many browsers<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup> nowadays offer the option to configure a custom DoH server.
However, this kind of configuration only applies to the activity happening in
the browser and as mentioned previously, not all web requests originate from a
browser. If we want to protect all DNS traffic emerging from a network, say a
user&rsquo;s home network, we should configure the network to use a local DNS server
under our control that will turn around and delegate DNS resolution to a trusted
upstream provider using DoH. First, let&rsquo;s see how we can build this local DNS
server.</p>
<h2 id="local-dns-server">Local DNS Server</h2>
<p>Cloudflare built a nice little application called <code>cloudflared</code> that converts
plain DNS queries to DoH. It&rsquo;s free, open-source and easy to run.  If you&rsquo;ve
read any other post on this blog you must&rsquo;ve realized how much I love
containers. So let&rsquo;s build a <code>cloudflared</code> Docker image to run as a local DNS
server. Unfortunately, Cloudflare doesn&rsquo;t offer an official Docker image for
<code>cloudflared</code> but we can make our own fairly easily. To do so, in a file named
<code>Dockerfile</code> put the following content:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt">1
</span><span class="lnt">2
</span><span class="lnt">3
</span><span class="lnt">4
</span><span class="lnt">5
</span><span class="lnt">6
</span><span class="lnt">7
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl"><span class="k">FROM</span><span class="s"> alpine:3.15</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span><span class="k">RUN</span> apk add --no-cache bash<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span><span class="k">RUN</span> wget -q https://github.com/cloudflare/cloudflared/releases/download/2022.1.2/cloudflared-linux-arm <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>	<span class="o">&amp;&amp;</span> chmod +x cloudflared-linux-arm <span class="se">\
</span></span></span><span class="line"><span class="cl"><span class="se"></span>    <span class="o">&amp;&amp;</span> mv /cloudflared-linux-arm /usr/local/bin/cloudflared<span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><p>Here, we&rsquo;re using <code>alpine</code> version 3.15 and <code>cloudflared</code> version 2022.1.2 but
more versions might have been released since this article was published. Feel
free to update these versions to the latest.</p>
<p>In order to use this Docker image we can build a <code>docker-compose</code> service to run
<code>cloudflared</code>. Alongside the previously created <code>Dockerfile</code>, let&rsquo;s make a new
file called <code>docker-compose.yml</code> with this content:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl">version: <span class="s2">&#34;3.8&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>services:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  cloudflared:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    build: .<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    ports:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="s2">&#34;53:5053&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># replare &lt;UPSTREAM SERVER&gt; with the URL for the upstream DoH server</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># e.g.: https://doh.libredns.gr/dns-query</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    entrypoint: bash -c <span class="s2">&#34;cloudflared proxy-dns --port 5053 --address 0.0.0.0 --upstream &lt;UPSTREAM SERVER&gt;&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    restart: unless-stopped<span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><p>Make sure to replace <code>&lt;UPSTREAM SERVER&gt;</code> in the <code>entrypoint</code> command with your
DoH server of choice. PrivacyGuides lists <a href="https://privacyguides.org/providers/dns/">a few options</a>
you can pick from if you don&rsquo;t already have a favourite DNS server. At this
point, you have everything you need to convert all DNS queries in your network
into DoH. Here are the steps to accomplish this:</p>
<ol>
<li>Run <code>docker-compose up cloudflared</code> from the folder where you put the
<code>Dockerfile</code> and <code>docker-compose.yml</code> files to spin up a <code>cloudflared</code>
container. Ideally, this should be done on a machine that&rsquo;s constantly
running on your network with a static IP address. A Raspberry Pi or a home
server are great candidates for this.</li>
<li>Configure your network&rsquo;s DHCP server to use the machine where <code>cloudflared</code>
is running as its default DNS server. This will automatically instruct all
devices on the network to use the <code>cloudflared</code> container for DNS resolution.
If you don&rsquo;t have control over your DHCP server, or it&rsquo;s too complicated to
reconfigure at the moment, you can always start off by manually editing the
DNS configuration on your most-used devices.</li>
</ol>
<h2 id="doh-in-pihole">DoH in Pihole</h2>
<p>If you already have a Pihole docker container running in your network and
serving DNS queries, you can now set the <code>cloudflared</code> container as an upstream
DNS server in Pihole and automatically upgrade all DNS queries to DoH. We&rsquo;ve
seen in <a href="/posts/pihole-dhcp-docker-bridge-network">a previous article</a> how to set up Pihole using Docker. We&rsquo;ll use the same
<code>docker-compose</code> file here to illustrate how to integrate <code>cloudflared</code>.</p>
<p>First we need to place the <code>Dockerfile</code> file we created in the previous section
inside a folder called <code>cloudflared</code>. Then, using our previous Pihole
<code>docker-compose</code> file, we can add a new service for <code>cloudflared</code> as shown
below:</p>
<div class="highlight"><div class="chroma">
<table class="lntable"><tr><td class="lntd">
<pre tabindex="0" class="chroma"><code><span class="lnt"> 1
</span><span class="lnt"> 2
</span><span class="lnt"> 3
</span><span class="lnt"> 4
</span><span class="lnt"> 5
</span><span class="lnt"> 6
</span><span class="lnt"> 7
</span><span class="lnt"> 8
</span><span class="lnt"> 9
</span><span class="lnt">10
</span><span class="lnt">11
</span><span class="lnt">12
</span><span class="lnt">13
</span><span class="lnt">14
</span><span class="lnt">15
</span><span class="lnt">16
</span><span class="lnt">17
</span><span class="lnt">18
</span><span class="lnt">19
</span><span class="lnt">20
</span><span class="lnt">21
</span><span class="lnt">22
</span><span class="lnt">23
</span><span class="lnt">24
</span><span class="lnt">25
</span><span class="lnt">26
</span><span class="lnt">27
</span><span class="lnt">28
</span><span class="lnt">29
</span><span class="lnt">30
</span><span class="lnt">31
</span><span class="lnt">32
</span><span class="lnt">33
</span><span class="lnt">34
</span></code></pre></td>
<td class="lntd">
<pre tabindex="0" class="chroma"><code class="language-docker" data-lang="docker"><span class="line"><span class="cl">version: <span class="s2">&#34;3.8&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>services:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  pihole:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    environment:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="nv">PIHOLE_DNS_</span><span class="o">=</span>172.31.0.200#5350<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    depends_on:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - cloudflared<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - dhcphelper<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  dhcphelper:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  cloudflared:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    build: ./cloudflared<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    environment:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      <span class="c1"># set UPSTREAM_PROVIDER to the URL for the upstream DOH server</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      <span class="c1"># e.g.: https://doh.libredns.gr/dns-query</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      - <span class="nv">UPSTREAM_PROVIDER</span><span class="o">=</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    entrypoint: bash -c <span class="s2">&#34;cloudflared proxy-dns --port 5053 --address 0.0.0.0 --upstream </span><span class="nv">$$</span><span class="s2">UPSTREAM_PROVIDER&#34;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    networks:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>      backend:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>        ipv4_address: <span class="s1">&#39;172.31.0.200&#39;</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>    restart: unless-stopped<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>networks:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  <span class="c1"># ...</span><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>volumes:<span class="err">
</span></span></span><span class="line"><span class="cl"><span class="err"></span>  <span class="c1"># ...</span><span class="err">
</span></span></span></code></pre></td></tr></table>
</div>
</div><p>You&rsquo;ll notice that the <code>cloudflared</code> service looks similar to the one we built
in the previous section. Here, we don&rsquo;t need to expose the port 53 to the host
because the only client connecting to the <code>cloudflared</code> container is the Pihole
container running in the same Docker network. Also, we have to set a static IP
address for the <code>cloudflared</code> container and add it to the <code>PIHOLE_DNS_</code>
environment variable of Pihole in order for it to be used as an upstream DNS
server.</p>
<p>With the modifications above, restart your Pihole container by running
<code>docker-compose down</code> and bringing it back up with <code>docker-compose up pihole</code>,
and you should have a <code>cloudflared</code> container running alongside Pihole, ready to
receive requests.</p>
<h2 id="wrap-up">Wrap up</h2>
<p>Congratulations, you made it! You now understand why protecting your DNS traffic
is paramount to improving your online privacy and have in your toolbox 3
strategies for doing so, some more potent than others. In case you can&rsquo;t run a
full-fledged Pihole and <code>cloudflared</code> setup on your network, or don&rsquo;t have the
time to set that up yet, at least configure your most-frequently-used browser to
resolve DNS through DoH. It takes almost no time, and goes a long way in getting
you to a better place when it comes to your online privacy.</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>Keep in mind that this only protects DNS queries. It is still possible for
an adversary to figure out the websites you visit by doing reverse DNS
resolution on the IP addresses of those websites.&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2">
<p>Here&rsquo;s an <a href="https://support.mozilla.org/en-US/kb/firefox-dns-over-https">example</a> for Firefox.&#160;<a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
