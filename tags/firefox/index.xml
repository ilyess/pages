<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Firefox on ilyess</title>
    <link>https://ilyess.cc/tags/firefox/</link>
    <description>Recent content in Firefox on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Sun, 22 May 2022 20:00:00 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/firefox/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Why I Love Firefox</title>
      <link>https://ilyess.cc/posts/why-i-love-firefox/</link>
      <pubDate>Sun, 22 May 2022 20:00:00 -0400</pubDate>
      <guid>https://ilyess.cc/posts/why-i-love-firefox/</guid>
      <description>&lt;p&gt;Long gone are the days when websites consisted of static HTML pages styled with
basic CSS, when links actually directed users to new web locations instead of
calling a Javascript function. Now, websites (or should I call them web apps?)
have grown so complex that browsers had no choice but to turn into operating
systems. This made for a much richer user experience with so many possibilities;
you can do online banking, play video games, join video conferences, shop, and
even install apps, all within the same application: the browser. It&amp;rsquo;s obvious
that the more complex any software gets, the more chances there are for security
vulnerabilities to emerge, and the browser is no exception.
&lt;a href=&#34;https://www.cvedetails.com/vulnerability-list.php?vendor_id=1224&amp;amp;product_id=15031&amp;amp;page=1&amp;amp;year=2021&#34;&gt;The surge of browser CVEs&lt;/a&gt; we&amp;rsquo;ve witnessed in recent years is a good testament
to that. Additionally, the more user interactions are made possible in the
browser the more attractive it becomes to marketers and companies looking for
new revenue streams. Online advertising, which is fueled by online tracking,
has become so lucrative that it&amp;rsquo;s consistently been &lt;a href=&#34;https://www.statista.com/statistics/1093781/distribution-of-googles-revenues-by-segment/&#34;&gt;the top revenue category&lt;/a&gt;
for so many big tech corporations. So, should we just throw our hands up in the
air and accept our imposed destiny? Do we have to give up our privacy if we&amp;rsquo;re
to do any meaningful browsing on the internet?&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>Long gone are the days when websites consisted of static HTML pages styled with
basic CSS, when links actually directed users to new web locations instead of
calling a Javascript function. Now, websites (or should I call them web apps?)
have grown so complex that browsers had no choice but to turn into operating
systems. This made for a much richer user experience with so many possibilities;
you can do online banking, play video games, join video conferences, shop, and
even install apps, all within the same application: the browser. It&rsquo;s obvious
that the more complex any software gets, the more chances there are for security
vulnerabilities to emerge, and the browser is no exception.
<a href="https://www.cvedetails.com/vulnerability-list.php?vendor_id=1224&amp;product_id=15031&amp;page=1&amp;year=2021">The surge of browser CVEs</a> we&rsquo;ve witnessed in recent years is a good testament
to that. Additionally, the more user interactions are made possible in the
browser the more attractive it becomes to marketers and companies looking for
new revenue streams. Online advertising, which is fueled by online tracking,
has become so lucrative that it&rsquo;s consistently been <a href="https://www.statista.com/statistics/1093781/distribution-of-googles-revenues-by-segment/">the top revenue category</a>
for so many big tech corporations. So, should we just throw our hands up in the
air and accept our imposed destiny? Do we have to give up our privacy if we&rsquo;re
to do any meaningful browsing on the internet?</p>
<p>Using a sound browser will go a long way in mitigating the risk of privacy
violations. Like anything else in life, it&rsquo;s important to use the right tool for
the job. If you&rsquo;re going on a road trip across the country, you&rsquo;d want to drive
the right vehicle, have spare tires, make sure the engine is healthy and won&rsquo;t
overheat, plan charging breaks if you&rsquo;re going electric, and so on.  The same
applies to browsing the internet. In this article, I&rsquo;ll go over different
security and privacy measures implemented in <a href="https://firefox.com">Firefox</a>, my favourite browser.</p>
<p>Before we dive in, let me note that my decision to run Firefox as my daily
driver is not a political one or based on Mozilla&rsquo;s management, work conditions,
or product line. I&rsquo;m only focused on the software itself and how it serves my
personal internet browsing needs.</p>
<h2 id="http-header-sanitization">HTTP header sanitization</h2>
<p>In this day and age, web pages have become remarkably complex to the point where
it&rsquo;s quite rare to find web services that don&rsquo;t pull resources from other
domains, may they be fonts, images, videos, scripts, or other types of
resources.  When a browser goes out to fetch external resources, it attaches the
original URL to every request in the <a href="https://en.wikipedia.org/wiki/HTTP_referer">HTTP Referer header</a>. The entire URL that
the user had initially put in their browser address bar ends up transmitted to
all the domains that the page being visited depends on. To give you an idea, if
a user was reading an article on a news site in dark mode with big font (URL:
<a href="https://www.allthenews.com/breaking-news-read-now?mode=dark&amp;font-size=big);">https://www.allthenews.com/breaking-news-read-now?mode=dark&amp;font-size=big);</a> and this site loaded &ldquo;share&rdquo;
buttons from Google, Twitter, and Facebook; all of these companies would be made
aware of the article the user was reading <em>and</em> the preferred settings for
article consumption.</p>
<p>Firefox <a href="https://blog.mozilla.org/security/2021/03/22/firefox-87-trims-http-referrers-by-default-to-protect-user-privacy/">trims the path and query parameters from the HTTP Referer header</a> for all
cross-origin requests by default. So in our previous example the URL would be
turned into <a href="https://www.allthenews.com/">https://www.allthenews.com/</a> when communicated as a referrer to
Google, Twitter, and Facebook, thereby reducing the amount of leaked user
information.</p>
<h2 id="cookie-store-isolation">Cookie store isolation</h2>
<p>When it comes to cookie management, Firefox has really stepped up their game. I
don&rsquo;t think any other browser handles cookies nearly as well.  Firefox not only
blocks cookies from domains that were identified as trackers, it totally
cripples cookie-based tracking with its Total Cookie Protection. It works by
operating completely isolated cookie jars, each dedicated to a website that the
user explicitly visited. By way of illustration, cookies set when the user
visits website A are only ever attached to requests triggered by user actions on
website A, including third-party cookies. This is exceptionally powerful because it
means that a third-party cookie coming from Facebook, for instance, while
browsing a news site will never make it back to Facebook when browsing any other
website that uses Facebook&rsquo;s assets.</p>
<p>In addition, Firefox&rsquo;s Enhanced Cookie Clearing (ECC) allows you to wipe out all
the data that were created by a specific website. This include first and third
party cookies, local storage data, settings, and cache. This is different than
the typical &ldquo;delete website cookies&rdquo; setting you&rsquo;ll find in other mainstream
bowsers in that ECC not only deletes data that belong to the selected website,
but also all other data that were saved in your browser while visiting that
website. Let&rsquo;s say you&rsquo;re on recipes.com that pulls in some assets
from google.com, while also being directly logged into google.com in a separate
tab. Clearing cookies of recipes.com using ECC will have the following effects:</p>
<ul>
<li>Delete all the data created by recipes.com</li>
<li>Delete all the data created by google.com <em>while browsing recipes.com</em>. This
means that we&rsquo;ll remain logged into google.com in the second tab. This is
extremely potent as it grants you the ability to make your browser
effectively forget about all your activity on a given website.</li>
</ul>
<h2 id="process-isolation">Process isolation</h2>
<p>Every time you visit a website with Javascript enabled, you run code that you&rsquo;ve
most likely never seen on your computer<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>. The vast majority of browsers do a
great job containing this code in a tight sandbox to prevent malicious
out-of-scope access, but not all enforce good segregation within the sandbox.
Firefox goes to great lengths to isolate code pulled from the internet that runs
on your machine.  With its <a href="https://hacks.mozilla.org/2021/05/introducing-firefox-new-site-isolation-security-architecture/">Site Isolation</a>, also called Project Fission, each
website is loaded in a separate Operating System (OS) process. So if you visit
&ldquo;example.com&rdquo; and, in a new tab, load &ldquo;example.org&rdquo;, these 2 websites&rsquo; code will
run in 2 OS-segregated processes, each with its own isolated memory. And this
isn&rsquo;t specific to tabs; if &ldquo;example.com&rdquo; had an iframe that loaded content from
&ldquo;example.org&rdquo;, the same thing would happen: Two processes would be spawned, one
for each domain. This drastically improves protection against timing attacks
like <a href="https://en.wikipedia.org/wiki/Spectre_(security_vulnerability)">Spectre</a> and <a href="https://en.wikipedia.org/wiki/Meltdown_(security_vulnerability)">Meltdown</a> where one process can illegitimately peak into
another&rsquo;s memory. To see Firefox processes type &ldquo;about:processes&rdquo; in the address
bar and hit &ldquo;Enter&rdquo;.</p>
<p>Moreover, Firefox is smart enough to also account for <a href="https://publicsuffix.org">Public Suffix List</a><sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup>
domains where multiple sites can be served as different subdomains of the same
domain.  For instance, if you were to load &ldquo;my-site.codeberg.page&rdquo; and
&ldquo;another-site.codeberg.page&rdquo;, they would each get a separate process because
they would be considered two different websites even though they share the same
domain, since this domain is part of the Public Suffix List.</p>
<h2 id="session-isolation">Session isolation</h2>
<p>Now, let&rsquo;s go up few layers and look at what Firefox does at the session level.
While there&rsquo;s no built-in session separation beyond Private Browsing in Firefox,
their team has built the <a href="https://blog.mozilla.org/en/products/firefox/introducing-firefox-multi-account-containers/">Multi-Account Containers</a> add-on which takes
compartmentalization to the next level. Every container is a fresh browser
session with its own storage and cookie store. Think &ldquo;Private Browsing&rdquo; but not
limited to a single private session. This is particularly convenient when you
want to log into multiple accounts on the same website. Furthermore, all the
security and privacy measures we touched on so far are maintained inside each
container. When you first visit a website in a container, Firefox remembers that
and asks you the next time around if you&rsquo;d like to assign that website to that
container. Replying yes makes Firefox always open that website in the chosen
container without you having to do so every time. Who said you had to pick
between privacy and convenience again?</p>
<p>You can also set a separate Virtual Private Network (VPN) per container. For
example, if you want to use a VPN when shopping online you could create a
&ldquo;shopping&rdquo; container and configure it to always use your VPN<sup id="fnref:3"><a href="#fn:3" class="footnote-ref" role="doc-noteref">3</a></sup>.</p>
<h2 id="profiles">Profiles</h2>
<p>To push compartmentalization even further, Firefox allows you to manage multiple
<a href="https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles?redirectslug=profile-manager-create-and-remove-firefox-profiles&amp;redirectlocale=en-US">profiles</a> within the same installation. A profile is nothing more than a set of
user information, like bookmarks, saved passwords, settings, etc. While using
profiles is generally not needed, there are few cases where it comes in very
handy. Imagine dealing with a website that doesn&rsquo;t work properly with Firefox
strict tracking protection mode, which you should always have on by the way. You
could, short of ditching that website and finding a better alternative, spin up
a new profile where you don&rsquo;t use strict tracking protection mode for the sole
purpose of visiting that website. This can apply to anything that&rsquo;s not impacted
by the Multi-Account Containers add-on like bookmarks, extensions, and themes.</p>
<h2 id="wrap-up">Wrap up</h2>
<p>Well that was a lot of material! We&rsquo;ve covered various security and privacy
features that come with Firefox out of the box, in addition to an add-on that
brings a new dimension of compartmentalization to the mix. Things like cookie
store, session, and process isolation; HTTP header sanitization; and more.
In my view, this makes Firefox the best browser, at the time of this writing,
for a privacy-conscious internet user like myself.</p>
<p>If you know a free and open-source browser that offers all the guarantees
mentioned in this article, please bring it to my attention; I&rsquo;d love to see how
it compares to Firefox.</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>Provided that said website uses Javasctipt&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2">
<p>Public Suffix List is a community-maintained list of effective top level
domains (eTLDs) that host different sites as subdomains, like github.io and
codeberg.page.&#160;<a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:3">
<p>This only applies to HTTP/HTTPS requests. DNS queries are still subject to
browser DNS settings and do not go through the VPN configured for the container.&#160;<a href="#fnref:3" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
