<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Security on ilyess</title>
    <link>https://ilyess.cc/tags/security/</link>
    <description>Recent content in Security on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Tue, 28 Dec 2021 20:00:00 -0500</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/security/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>My Journey with Password Managers</title>
      <link>https://ilyess.cc/posts/my-journey-with-password-managers/</link>
      <pubDate>Tue, 28 Dec 2021 20:00:00 -0500</pubDate>
      <guid>https://ilyess.cc/posts/my-journey-with-password-managers/</guid>
      <description>&lt;p&gt;Password managers have become a crucial tool that every online user must rely
on. Since they handle extremely sensitive data, like login credentials, credit
card information, secret notes, and more, careful consideration is recommended
when choosing a password manager. This article goes over the phases I&amp;rsquo;ve gone
through in my experience with password managers, the different solutions I used,
and how I got where I am today.&lt;/p&gt;
&lt;h2 id=&#34;first-password-manager&#34;&gt;First Password Manager&lt;/h2&gt;
&lt;p&gt;My journey with password managers started way back when I used the &amp;ldquo;save
password&amp;rdquo; feature in the Chrome browser. I was still &amp;ldquo;generating&amp;rdquo; passwords
myself but relying on Chrome to save and automatically insert them when I&amp;rsquo;m on
the login page of various websites. At that time, I didn&amp;rsquo;t really have too many
accounts - probably no more than a dozen. But as the web started turning into a
plethora of sign-up walls, where you can&amp;rsquo;t hover on a button without having to
log in first, I quickly ran out of ideas to create new unique
passwords. This is the point where I knew I needed a proper password manager.&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>Password managers have become a crucial tool that every online user must rely
on. Since they handle extremely sensitive data, like login credentials, credit
card information, secret notes, and more, careful consideration is recommended
when choosing a password manager. This article goes over the phases I&rsquo;ve gone
through in my experience with password managers, the different solutions I used,
and how I got where I am today.</p>
<h2 id="first-password-manager">First Password Manager</h2>
<p>My journey with password managers started way back when I used the &ldquo;save
password&rdquo; feature in the Chrome browser. I was still &ldquo;generating&rdquo; passwords
myself but relying on Chrome to save and automatically insert them when I&rsquo;m on
the login page of various websites. At that time, I didn&rsquo;t really have too many
accounts - probably no more than a dozen. But as the web started turning into a
plethora of sign-up walls, where you can&rsquo;t hover on a button without having to
log in first, I quickly ran out of ideas to create new unique
passwords. This is the point where I knew I needed a proper password manager.</p>
<p>Around this time, <a href="https://www.lastpass.com">Lastpass</a> was one of the most dominant actors in the field of
password management and they were doing some good work. So I decided to give
Lastpass a try and I really liked it. The convenience of not having to come up
with a &ldquo;secure&rdquo; password that I haven&rsquo;t used before when signing up on a new
website, brought such a relief to my account creation flow and reduced a
considerable amount of friction. Lastpass had some issues however.
<a href="https://www.wired.com/2015/06/hack-brief-password-manager-lastpass-got-breached-hard/">The data breach of 2015</a>
was the first thing to sound the alarm for me. I started to realize
that fully trusting an entity with my most valuable data, the keys to every
single account I own, was probably not a good idea. To make matters more scary,
Lastpass does not publish their source code for others to view, analyze, and
even contribute to. Security by obscurity is never the way to go.
Furthermore, <a href="https://www.theverge.com/2021/2/26/22302709/lastpass-android-app-trackers-security-research-privacy">they include third-party trackers</a>
in their codebase which is a very bad practice for a security-critical service
like Lastpass.</p>
<p>I&rsquo;m not trying to sabotage Lastpass here, or imply that their security is
lacking. As a matter of fact, their security model seems to be solid. As far
as I know, there has never been any breach that exposed user sensitive data like
passwords in clear text. All I&rsquo;m saying is that it wasn&rsquo;t the right fit for me.
Not to mention that their recent <a href="https://blog.lastpass.com/2021/02/changes-to-lastpass-free/">up-sell push</a>
to convert free users to premium by restructuring their pricing and feature
models just didn&rsquo;t sit right with me.  They were turning into another Big Tech
player, and it was time for me to walk away.</p>
<h2 id="gaining-back-control-over-my-data">Gaining back control over my data</h2>
<p>When I started using Lastpass, there weren&rsquo;t many options to choose from. But
few years later, other password managers started popping up and one of them was
particularly good at distinguishing itself amongst the privacy-focused
community. This password manager is no other than <a href="https://keepassxc.org/">KeepassXC</a>. I believe it&rsquo;s the
first open-source password manager that I was made aware of and I instantly fell
for it.  It&rsquo;s a free, community-driven, offline password manager that I simply
couldn&rsquo;t ignore. Next thing you know, I was migrating all of my passwords off of
Lastpass and celebrating the addition of yet another open-source tool to my
arsenal.</p>
<p>The only problem that I had to solve was synchronizing data across multiple
devices. With a cloud-based service like Lastpass, this issue is taken care of
by the service itself and changes on one device are replicated across the others
automatically. With an offline password manager like KeepassXC on the other
hand, data synchronization becomes the responsibility of the user. So, I moved
all my credentials to a KeepassXC database (DB), and used a cloud service to
regularly back it up.  When I needed a fresh version of my DB in any of my
devices, I either transferred it directly within my home network, or pulled it
from the cloud service provider, and used an open-source client to read it. To
make things simple, I also only ever made changes to the DB on my computer, and
stuck to read-only mode on the other devices. You can imagine how this could
sometimes prove tedious, especially when I needed to update existing or create
new entries in my credentials list from a device other than my main computer. I
started to seriously miss the convenience of the seamless synchronization I
enjoyed with Lastpass, but I wasn&rsquo;t ready to downgrade my security and privacy
just for that.</p>
<h2 id="the-best-of-all-worlds">The best of all worlds</h2>
<p>Luckily, there was this new kid on the block, a new solution that ticked all the
boxes for me - it&rsquo;s open source, free<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>, and self-hostable! This meant that I
could get all the benefits of data control I have with KeepassXC <em>and</em> the
convenience of seamless multi-device synchronization. The solution I&rsquo;m referring
to is <a href="https://bitwarden.com/">Bitwarden</a>. Ever since I launched <a href="/posts/self-host-bitwarden-using-docker">my own instance of Bitwarden</a>,
I&rsquo;ve never looked back. I truly love this piece of software and am so grateful
to the amazing people behind it. Plus, the beauty of it all is that when you
host your own instance you get all of the premium features for free!</p>
<h2 id="wrap-up">Wrap up</h2>
<p>As you see, I wasn&rsquo;t fortunate enough to pick a winner right from the start. I
began my journey naive, prioritizing convenience without paying much attention
to security, let alone privacy. As I learned more about password managers, I
shifted my priorities and started giving more weight to security and privacy,
even at the expense of convenience at times.</p>
<p>If you have comments or suggestions, or if you just want to strike a
conversation on this topic, feel free to hit me up on <a href="https://mastodon.online/@ilyess">Mastodon</a>.</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>They also offer premium plans but their free tier has all the basic
features expected in a password manager.&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
