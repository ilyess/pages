<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Notes on ilyess</title>
    <link>https://ilyess.cc/tags/notes/</link>
    <description>Recent content in Notes on ilyess</description>
    <generator>Hugo -- 0.140.2</generator>
    <language>en</language>
    <lastBuildDate>Thu, 19 Jan 2023 20:00:00 -0400</lastBuildDate>
    <atom:link href="https://ilyess.cc/tags/notes/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>My Journey With Note Apps</title>
      <link>https://ilyess.cc/posts/my-journey-with-note-apps/</link>
      <pubDate>Thu, 19 Jan 2023 20:00:00 -0400</pubDate>
      <guid>https://ilyess.cc/posts/my-journey-with-note-apps/</guid>
      <description>&lt;p&gt;Everybody needs to take notes, may it be for school, to have a handy grocery list in the store, or
to keep a log of business expenses for accounting purposes. Some people use physical notebooks, some
pick up whatever napkin happens to be in the immediate vicinity and roll with it for the day, and
others, like myself, prefer to leverage digital notes. In this article, I&amp;rsquo;ll take you through my
journey with note taking apps. We&amp;rsquo;ll go over my personal definition of a &amp;ldquo;good&amp;rdquo; note taking app,
explore some of the options I used and the challenges I faced, and finally land on the solution I
ended up adopting with a couple of learnings that emerged along the way.&lt;/p&gt;</description>
      <content:encoded><![CDATA[<p>Everybody needs to take notes, may it be for school, to have a handy grocery list in the store, or
to keep a log of business expenses for accounting purposes. Some people use physical notebooks, some
pick up whatever napkin happens to be in the immediate vicinity and roll with it for the day, and
others, like myself, prefer to leverage digital notes. In this article, I&rsquo;ll take you through my
journey with note taking apps. We&rsquo;ll go over my personal definition of a &ldquo;good&rdquo; note taking app,
explore some of the options I used and the challenges I faced, and finally land on the solution I
ended up adopting with a couple of learnings that emerged along the way.</p>
<h2 id="the-beginning">The Beginning</h2>
<p>My journey with digital note taking started with whatever default notes app happened to be on my
phone. At first, I didn&rsquo;t have any particular framework for note taking; it was just a random
collection of digital post-it stickers. Similar story on the desktop, a structureless heterogeneous
cluster of text files scattered all over the place.</p>
<p>Then, I started taking notes more seriously (pun intended) to capture knowledge, such as book
summaries and highlights, things I learn from videos and documentaries, new vocabulary I come
across, random ideas or questions I want to get answers to, and more. Before you know it, my notes
catalogue started getting a bit heavy and it was apparent that I needed some sort of framework to
better tag and organize all that knowledge. At the time, <a href="https://evernote.com">Evernote</a> was the new kid on the block and
it had some attractive features, so without putting much thought into it, I created an account and
poured all my notes over to it.</p>
<p>I used Evernote for a long time, longer than I would want to admit. But one day, shortly after I&rsquo;d
begun getting serious about my online privacy, I had a &ldquo;Oh shit!&rdquo; moment that I still remember to
this day.</p>
<h2 id="the-rabbit-hole">The Rabbit Hole</h2>
<p>I geared up and I ventured down the rabbit hole of privacy-friendly, and open-source, note taking
apps. I set a few criteria to guide my expedition:</p>
<ol>
<li>The service has to be free of charge, or offer a free plan;</li>
<li>It has to be offline and/or support end-to-end encrypted backup and/or sync; and</li>
<li>It must respect user privacy, so no tracking, ads, or telemetry.</li>
</ol>
<p>If there&rsquo;s one thing I would never accept, it would be having my notes up in the cloud in the
clear<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>, may that be temporarily for synchronization or permanently for backup. My notes hold so
much information about me, from future plans, to areas of interest in life, to just general
knowledge. I bet that even my IQ could be deduced with a reasonable degree of certainty using the
right AI model. Now that I think of it, I would definitely see myself in the future training a model
on my notes and delegating some decision making to it: a tailored virtual assistant of sort. I&rsquo;m no
AI expert and this does seem far-fetched today, but I wouldn&rsquo;t be surprised if it were a thing even
half a decade from now. Anyway, I digress.</p>
<p>During my research, <a href="https://standardnotes.com">StandardNotes</a> stood out as a good candidate with its end-to-end encrypted backup
and sync, and focus on privacy. It seemed to check all the boxes, so I gave it a shot. I created a
free account and, once again, migrated all my notes off of Evernote into my StandardNotes account. I
have to admit that this immediately felt like a downgrade; the feature set of StandardNotes&rsquo; free
tier is rudimentary at best. But in the name of privacy, I sucked it up and worked around the
limitations for a while. Thinking back, I realize that this was more of a &ldquo;stop the bleeding&rdquo; step
than a permanent solution. Case in point, I never actually stopped exploring the free and open
source note taking space for better options. Then, I came across <a href="https://joplinapp.org">Joplin</a>.</p>
<p>At first, Joplin&rsquo;s UI/UX looked out-dated, especially on mobile devices, but I was willing to be
more lenient given that it&rsquo;s a free and open source software maintained by a handful of generous
people. What grabbed my attention was its self-hostable end-to-end encrypted capability. So I
downloaded a copy of the app, set up a WebDAV server to handle the sync, and migrated all my notes
over. After a few weeks of me getting used to its archaic UI, Joplin began setting itself as my
permanent note taking solution. I started tinkering with various plugins, and even looked into
making my own. All was great until the day the sync broke! My client was getting 503 errors from the
sync server which corrupted some notes and rendered them unusable. Luckily, I had an offline backup
of the affected files, so I managed to recover, albeit laboriously, from this crash. A few weeks
later, another sync crash wiped my <em>entire</em> notebook. I wasn&rsquo;t so lucky this time around, however,
and ended up permanently losing some of my newer notes. This was the straw that broke the camel&rsquo;s
back. I couldn&rsquo;t take it anymore; I had to find a better solution.</p>
<p>I decided to give StandardNotes another shot, but going with a self-hosted instance this time. This
will ensure that I have full control over my notes&rsquo; backups, and that I enjoy a more decent set of
features; or so I thought. So I migrated all my notes, once more, to my <a href="/posts/self-host-standard-notes-with-premium-extensions">StandardNotes instance</a> and
started playing around with premium themes and extensions. The fact that a simple notes service
required so many backend services, a.k.a micro-services, has never really sat well with me. I had
the feeling that this level of complexity will eventually prove too cumbersome to maintain. And
guess what? It did! After a recent update of the desktop client, my setup was brought to a halt. I
was no longer able to connect to my self-hosted instance, running an old version of the server. It
seemed like the new client version wasn&rsquo;t backward-compatible and required a backend upgrade. When I
tried to make that happen, I realized that the StandardNotes&rsquo; team overhauled their entire
architecture. At this point, I wasn&rsquo;t really down to learn the new setup, build new docker images,
and get everything back online. It felt like a lot more work than a simple notes service should call
for.</p>
<h2 id="refocusing">Refocusing</h2>
<p>I took a step back, and went back to the drawing board. A quick cost/reward analysis confirmed that
all of this friction wasn&rsquo;t worth it. I realized that I didn&rsquo;t <em>really</em> need the cross-device sync,
since the vast majority of my notes fell under 2 categories: desktop, and phone. I seldom need notes
from my phone when I&rsquo;m on the desktop, and I almost never need notes from my desktop when I&rsquo;m on my
phone. So I decided to use 2 different note taking applications: <a href="https://www.vim.org">vim</a> with <a href="https://vimwiki.github.io">vimwiki</a> for desktop notes,
and my phone&rsquo;s default notes app with cloud backup disabled. I suppose you know the drill by now: I
migrated all of my notes, yes once again, from StandardNotes to vim and my phone&rsquo;s default note app,
hoping that this was the last time I perform this dance.</p>
<p>I&rsquo;ve been driving this setup for weeks now and so far have a couple of takeaways. First, I&rsquo;m struck
by how pleasant it is to enter a typo&rsquo;ed keyword into a search bar and watch the powerful fuzzy
search engine instantly populate the page with extremely relevant results, the first one being the
one I had in mind most of the time. I can&rsquo;t remember a single time where this failed me. This made
me realize just how mediocre, if not completely futile, StandardNotes&rsquo; search functionality was,
particularly on mobile. Second, I never <em>actually</em> needed the cross-device sync or cloud backup for
my notes. Offline backup, and segregated notebooks work just fine, check all the boxes with regards
to privacy, and cost me practically no effort.</p>
<p>Before closing this off, I&rsquo;d like to mention a couple other note services that I considered at some
point, but wasn&rsquo;t entirely sold on so I never took them for a test drive. <a href="https://obsidian.md">Obsidian</a> is more than just
a note taking app. With its graph features and automatic note linking, it appears to be more of a
knowledge management software; way more than I actually need for my personal use. Plus, the sync is
a paid service with no option to self-host. <a href="https://logseq.com">Logseq</a> is another solution that briefly caught my
attention until I learned that it had no native sync support; the only way to sync data was to go
through Github or iCloud.</p>
<h2 id="wrap-up">Wrap up</h2>
<p>If there&rsquo;s a lesson to be drawn from this journey, it has to be that the most complex solution isn&rsquo;t
always the right one. I would argue that the simplest one turns out to be the best, more often than
not. It just took me multiple migrations, hours if not days of tinkering around with various setups,
and quite a bit of frustration wrestling with poor-UX software and dealing with lousy error
handling, to realize it.</p>
<div class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1">
<p>I consider &ldquo;in the clear&rdquo; data any encrypted data where the keys belong to or are managed by
the server. Put differently, in this context, if it&rsquo;s not end-to-end encrypted, it&rsquo;s in the clear.&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</div>
]]></content:encoded>
    </item>
  </channel>
</rss>
